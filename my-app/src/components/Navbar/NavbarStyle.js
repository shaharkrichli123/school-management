import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(() => ({
    weatherNavbar: {
        height: '4.75rem',
        flexDirection: 'row !important',
        alignItems: 'center'
    },
    ProjTitle: {
        margin: '0 1rem'
    },
    navbarMenuIcon: {
        marginLeft: '1rem'
    }
  }));

  export default useStyles;