import React from "react";
import AppBar from "@material-ui/core/AppBar";
import Typography from "@material-ui/core/Typography";
import useStyles from "./NavbarStyle";
import PaletteIcon from "@mui/icons-material/Palette";
import Sidebar from "../Sidebar/Sidebar";

const Navbar = () => {
  const classes = useStyles();

  return (
    <div>
      <AppBar position="static" className={classes.weatherNavbar}>
        <Sidebar />
        <Typography classes={{ h5: classes.ProjTitle }} variant="h5">
          Shob Classes
        </Typography>
        <PaletteIcon />
      </AppBar>
    </div>
  );
};

export default Navbar;
