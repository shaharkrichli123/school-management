import React from "react";
import sidebarOptions from "@jsons/SidebarOptions";
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemText from '@mui/material/ListItemText';

const SidebarOptionList = () => {
  return (
    <div>
      <List>
        {sidebarOptions.options.map((option, index) => (
          <ListItem button key={index}>
            <ListItemText primary={option} />
          </ListItem>
        ))}
      </List>
    </div>
  );
};

export default SidebarOptionList;
