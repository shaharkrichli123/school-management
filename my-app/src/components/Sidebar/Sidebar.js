import React, { useState } from "react";
import Drawer from "@material-ui/core/Drawer";
import SidebarOptionList from "./SidebarOptionList";
import MenuIcon from "@mui/icons-material/Menu";
import useStyles from "./SidebarStyle";

const Sidebar = () => {
  const classes = useStyles()

  const [isSidebarOpen, setSidebarOpenInd] = useState(false);

  const toggleSidebar = () => {
    setSidebarOpenInd(isSidebarOpen => !isSidebarOpen)
  }

  return (
    <div>
      <MenuIcon onClick={toggleSidebar} className={classes.navbarMenuIcon} />
      <Drawer anchor="left" open={isSidebarOpen} onClose={toggleSidebar}>
        <SidebarOptionList />
      </Drawer>
    </div>
  );
};

export default Sidebar;
