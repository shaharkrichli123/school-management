import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(() => ({
    navbarMenuIcon: {
        marginLeft: '1rem',
        cursor: 'pointer'
    }
  }));

  export default useStyles;